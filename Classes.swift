class Animal {
    var name = "Noname"
    var height = 0
    var sound = ""
}

class Fish: Animal {
    var maxDepth = 0
}

class Bird: Animal {
    var wingspan = 0
}

class House {
    var width: Int
    var height: Int
    
    func create() {
        print(self.width * self.height)
    }
    
    func destroy() {
        print("House has been destroyed")
    }
    
    init (height: Int, width: Int) {
        self.height = height
        self.width = width
    }
}

let house = House(height: 2, width: 3)
house.create()
house.destroy()

class Student {
    var studentsArray = ["Ivan", "Vladimir", "Amir", "Peter", "Johnny"]
    
    func sortByAlphabet() -> [String] {
        return studentsArray.sorted{$0 < $1}
    }
    func sortByLength() -> [String] {
        return studentsArray.sorted{$0.count < $1.count}
    }
}

let students = Student()
print(students.sortByAlphabet())
print(students.sortByLength())

import Foundation
struct Point {
    var x: Int
    var y: Int

    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
    
    func distanceToOrigin() -> Double {
        return sqrt(Double(x * x + y * y))
    }
}

class Person {
    var name: String
    var age: Int

    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }

    func introduce() {
        print("Меня зовут \(name), мне \(age) лет.")
    }
}
// Отличия классов от структур:
// Наследование позволяет одному классу наследовать характеристики другого
// Приведение типов позволяет проверить и интерпретировать тип экземпляра класса в процессе выполнения
// Деинициализаторы позволяют экземпляру класса освободить любые ресурсы, которые он использовал
// Подсчет ссылок допускает более чем одну ссылку на экземпляр класса.