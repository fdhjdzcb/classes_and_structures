import Foundation

// Создаем колоду из 52 карт
var deck = [(String, Int)]()
let suits = ["пики", "️червы", "️крести", "️бубны"]
let ranks = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
for suit in suits {
    for rank in ranks {
        deck.append((suit, rank))
    }
}

// Выбираем 5 случайных карт из колоды
var hand = [(String, Int)]()
for _ in 0 ..< 5 {
    let index = Int.random(in: 0 ..< deck.count)
    hand.append(deck.remove(at: index))
}

// Сортируем карты по рангу
hand.sort { $0.1 < $1.1 }
print(hand)

// Проверяем комбинации
var combination: String?
if isRoyalFlush(hand: hand) {
    combination = "флеш-рояль"
} else if isStraightFlush(hand: hand) {
    combination = "стрит-флеш"
} else if isFourOfAKind(hand: hand) {
    combination = "каре"
} else if isFullHouse(hand: hand) {
    combination = "фулл-хаус"
} else if isFlush(hand: hand) {
    combination = "флеш"
} else if isStraight(hand: hand) {
    combination = "стрит"
} else if isThreeOfAKind(hand: hand) {
    combination = "сет"
} else if isTwoPair(hand: hand) {
    combination = "две пары"
} else if isOnePair(hand: hand) {
    combination = "пара"
} else {
    combination = "нет комбинации"
}

// Выводим результат
if let combination = combination {
    print("У вас \(combination)")
} else {
    print("У вас нет комбинации")
}

// Функции для проверки комбинаций
func isRoyalFlush(hand: [(String, Int)]) -> Bool {
    return isStraightFlush(hand: hand) && (hand[4].1 == 14)
}

func isStraightFlush(hand: [(String, Int)]) -> Bool {
    return isFlush(hand: hand) && isStraight(hand: hand)
}

func isFourOfAKind(hand: [(String, Int)]) -> Bool {
    let groups = Dictionary(grouping: hand, by: { $0.1 })
    return groups.values.contains { $0.count == 4 }
}

func isFullHouse(hand: [(String, Int)]) -> Bool {
    return isThreeOfAKind(hand: hand) && isOnePair(hand: hand)
}

func isFlush(hand: [(String, Int)]) -> Bool {
    return hand.allSatisfy { $0.0 == hand[0].0 }
}

func isStraight(hand: [(String, Int)]) -> Bool {
    for i in 1 ..< 5 {
        if hand[i].1 - hand[i - 1].1 != 1 {
            return false
        }
    }
    return true
}

func isThreeOfAKind(hand: [(String, Int)]) -> Bool {
    let groups = Dictionary(grouping: hand, by: { $0.1 })
    return groups.values.contains { $0.count == 3 }
}

func isTwoPair(hand: [(String, Int)]) -> Bool {
    let groups = Dictionary(grouping: hand, by: { $0.1 })
    return groups.values.filter {$0.count == 2}.count == 2
}

func isOnePair(hand: [(String, Int)]) -> Bool {
    let groups = Dictionary(grouping: hand, by: { $0.1 })
    return groups.values.contains { $0.count == 2 }
}